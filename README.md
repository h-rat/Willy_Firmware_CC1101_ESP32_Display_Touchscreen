# Introduction<a id="introduction"></a>
Willy is an alternative to Flipper Zero based on an ESP32.

<div align="flex">
  <img src="Image/IMG_20230624_181105.jpg" width="500" alt="Willy">
</div>

<br>

<strong>Idea, development and implementation of this firmware:</strong> h-RAT
<br>

# Summary

<li><strong><a href="#introduction">Introduction</a></strong></li><br>
<li><strong><a href="#features">Features</a></strong></li>
<ul>
<li><a href="#subghz">SubGhz</a></li>
<li><a href="#infrared">Infrared</a></li>
</ul>

<li><strong><a href="#video">Video</a></strong></li><br>
<li><strong><a href="#build">Build</a></strong></li><br>
<li><strong><a href="#buy">Buy</a></strong></li><br>
<li><strong><a href="#contact">Contact</a></strong></li><br>
<li><strong><a href="#disclaimer">Disclaimer</a></strong></li>

# Features<a id="features"></a>

<h2>SubGhz -> Record</h2><a id="subghz"></a>

- [x] Record Signal
<div>
  <img src="Image/Record.png" width="170" alt="Willy">
</div>

- [x] Send Last Signal
- [x] Save Last Signal	
<div>
  <img src="Image/Signal.png" width="170" alt="Willy">
</div>
   
<h2>SubGhz -> Transmit</h2>

- [x] Send Key*
<div>
  <img src="Image/Transmit.png" width="170" alt="Willy">
</div>

<h2>*Protocol List</h2>

- [x] Ansonic (12bit)
- [x] BETT (18bit)
- [x] CAME (12bit, 18bit, 24bit, 25bit)
- [x] Clemsa (18bit)
- [x] Doitrand (37bit)
- [x] Dooya (40bit)
- [x] FAAC SLH (64bit)
- [x] GateTX (24bit)
- [x] Holtek HT12X (12bit)
- [x] Holtek (40bit)
- [x] Honeywell (48bit)
- [x] Hormann (44bit)
- [x] IntertechnoV3 (32bit)
- [x] KeeLoq (64bit)
- [x] Kinggates (89bit)
- [x] LinearDelta3 (8bit)
- [x] Linear (10bit)
- [x] Magellan (32bit)
- [x] Marantec (49bit)
- [x] Nero Radio (56bit)
- [x] Nero Sketch (40bit)
- [x] Nice FLO (12bit, 24bit)
- [x] PhoenixV2 (52bit)
- [x] Power Smart (64bit)
- [x] Princeton (24bit)
- [x] Security+ V1 (21bit)
- [x] SMC5326 (25bit)
- [x] Starline (64bit)
- [x] UNILARM (25bit)

<h2>SubGhz -> Bruteforce</h2>

- [x] Bruteforce Key**
<div>
  <img src="Image/Bruteforce.png" width="170" alt="Willy">
</div>

<h2>**Protocol List</h2>

- [x] Ansonic (12bit)
- [x] CAME (12bit)
- [x] Chamberlain (7bit)
- [x] Chamberlain (8bit)
- [x] Chamberlain (9bit)
- [x] Holtek HT12X AM (12bit)
- [x] Holtek HT12X FM (12bit)
- [x] LinearDelta3 (8bit)
- [x] Linear (10bit)
- [x] Nice FLO (12bit)
- [x] Princeton (24bit)
- [x] SMC5326 (25bit)
- [x] UNILARM (25bit)

<h2>SubGhz -> Tesla</h2>

- [x] Send EU Tesla Charge Door
- [x] Send US Tesla Charge Door
<div>
  <img src="Image/Tesla.png" width="170" alt="Willy">
</div>

<h2>SubGhz -> DeBruijn</h2>

- [x] Send Linear Multicode
- [x] Send Stanley Multicode
- [x] Send Charmberlain
- [x] Send Linear MooreMatic
<div>
  <img src="Image/DeBruijn.png" width="170" alt="Willy">
</div>

<h2>SubGhz -> Jukebox</h2>

- [x] Send Free Credit
- [x] Send Pause Song
- [x] Send Skip Song
- [x] Send Volume UP
- [x] Send Volume DOWN
- [x] Send Power OFF
- [x] Send Lock Queue
<div>
  <img src="Image/Jukebox.png" width="170" alt="Willy">
</div>

<h2>SubGhz -> Attack Switch Mode</h2>

- [x] Receiver/Jammer mode (Need two devices to perform the attacks properly)
<div>
  <img src="Image/Attack.png" width="170" alt="Willy">
  <img src="Image/JammerRolljam.png" width="170" alt="Willy">
</div>

<h2>SubGhz -> Attack : Receiver : Rolljam</h2>

- [x] Set Jammer Power
- [x] Set Send First Signal Auto.
- [x] Start Rolljam Attack
- [x] Send First Signal
- [x] Send Second Signal
<div>
  <img src="Image/Rolljam.png" width="170" alt="Willy">
  <img src="Image/ConfigRolljam.png" width="170" alt="Willy">
</div>

<h2>SubGhz -> Attack : Receiver : Rollback</h2>

- [x] Set Jammer Power
- [x] Set Num. Signal Required
- [x] Set Time Frame
- [x] Start Rollback Attack
- [x] Send Sequence
- [x] Save Sequence
<div>
  <img src="Image/Rollback.png" width="170" alt="Willy">
  <img src="Image/ConfigRollback.png" width="170" alt="Willy">
</div>

<h2>SubGhz -> Jammer</h2>

- [x] Set Jammer Power
<div>
  <img src="Image/Jammer.png" width="170" alt="Willy">
</div>

<h2>SubGhz -> Scanner</h2>

- [x] Start Hoop Freq Scan
- [x] Set Frequency Found
<div>
  <img src="Image/Scanner.png" width="170" alt="Willy">
</div>

<h2>SubGhz -> SD Card</h2>

- [x] Send .sub file***
<div>
  <img src="Image/SD.png" width="170" alt="Willy">
</div>

<h2>***Protocol List</h2>

- [x] RAW
- [x] AlutechAT
- [x] Ansonic
- [x] BETT
- [x] CAME 
- [x] Clemsa
- [x] Doitrand
- [x] Dooya
- [x] FAAC SLH
- [x] GateTX
- [x] Holtek HT12X
- [x] Holtek
- [x] Honeywell
- [x] Hormann
- [x] IntertechnoV3
- [x] KeeLoq
- [x] Kinggates
- [x] LinearDelta3
- [x] Linear
- [x] Magellan
- [x] Marantec
- [x] Nero Radio
- [x] Nero Sketch
- [x] Nice FLO
- [x] PhoenixV2
- [x] Power Smart
- [x] Princeton
- [x] Security+ 1.0
- [x] Security+ 2.0
- [x] SMC5326
- [x] Starline
- [x] UNILARM

<h2>SubGhz -> Settings</h2>

- [x] Set Frequency
- [x] Set Preset
<div>
  <img src="Image/Settings.png" width="170" alt="Willy">
</div>

<h2>Infrared -> Universal Remotes</h2><a id="infrared"></a>

- [x] TV
<div>
  <img src="Image/TV.png" width="170" alt="Willy">
</div>

- [x] Digital Signs
<div>
  <img src="Image/DigitalsSigns.png" width="170" alt="Willy">
</div>

- [x] Projectors
<div>
  <img src="Image/Projectors.png" width="170" alt="Willy">
</div>

- [x] Audio
<div>
  <img src="Image/Audio.png" width="170" alt="Willy">
</div>

- [x] LED
<div>
  <img src="Image/LED.png" width="170" alt="Willy">
</div>

- [x] Fans
<div>
  <img src="Image/Fans.png" width="170" alt="Willy">
</div>

<h2>Infrared -> Learn</h2>

- [x] Receive IR Signal*
- [x] Save Last IR Signal
- [x] Send Last IR Signal
<div>
  <img src="Image/Learn.png" width="170" alt="Willy">
  <img src="Image/SignalIR.png" width="170" alt="Willy">
</div>

<h2>*Protocol List</h2>

- [x] NEC / APPLE / ONKYO
- [x] Kaseikyo
- [x] Sony
- [x] Samsung
- [x] RC5
- [x] RC6

<h2>Device</h2>

- [x] Set Theme Color
- [x] Device Information
<br><br><div>
  <img src="Image/Function.png" width="170" alt="Willy">
  <img src="Image/Infrared.png" width="170" alt="Willy">
  <img src="Image/Universal.png" width="170" alt="Willy">
  <img src="Image/Main.png" width="170" alt="Willy">
  <img src="Image/Misc.png" width="170" alt="Willy">
  <img src="Image/Theme.png" width="170" alt="Willy">
  <img src="Image/Device.png" width="170" alt="Willy">
</div>

# Video<a id="preview"></a>

  Demo: https://www.youtube.com/watch?v=r5-hpoPlQkU
  <br>
  Demo: https://www.youtube.com/shorts/5iebprjgEjE
  
# Build<a id="build"></a>

<div align="flex">
  <img src="Image/IMG_20230907_140559.jpg" width="500" alt="Willy">
</div>

<br>

Board:
- [x] 1x ESP32-S3 T-Display Touch Version - Soldered | https://www.aliexpress.us/item/3256804741686185.html

CC1101 Shield:
- [x] 1x S3 T-Display TF/SD Shield - Female | https://www.aliexpress.us/item/3256804956138540.html
- [x] 1x CC1101 V2.0 Module | https://www.aliexpress.us/item/2251832873028557.html
 
IR Shield:
- [x] 1x S3 T-Display TF/SD Shield - Female | https://www.aliexpress.us/item/3256804956138540.html
- [x] 1x Infrared Receiver TSOP4838 ( or any IR receiver model ) | https://www.aliexpress.us/item/1005004481948853.html
- [x] 1x Infrared LED TSAL6400 ( or any IR LED model ) | https://www.aliexpress.us/item/32670031000.html
- [x] 1x Transistor 2N4401 (Optional) | https://www.aliexpress.us/item/1005005484591463.html

# Buy<a id="buy"></a>

Due to a lot of abuse, the firmware is no longer free.

Firmware: https://willyfirmware.mysellix.io/product/64ca6015d3acc

Complete device: https://willyfirmware.mysellix.io/product/6509a82a3cec7
<br>

Add me on discord for more info.

# Contact<a id="contact"></a>

- [x] Discord: h_rat

# Disclaimer<a id="disclaimer"></a>

This device is a basic device for professionals and cybersecurity enthusiasts.

We are not responsible for the incorrect use of this device.

Be careful with this device and the transmission of signals. Make sure to follow the laws that apply to your country.
